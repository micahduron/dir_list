#include "DirTree.h"


void CDirTreeEntryList::appendChild(CDirTreeEntry child) {
    m_children.push_back(std::move(child));
}
void CDirTreeEntryList::swap(CDirTreeEntryList& swapee) {
    m_children.swap(swapee.m_children);
}

void CDirTreeEntry::assign(const QFileInfo& fileInfo) {
    m_fileName = fileInfo.fileName();
    m_fileInfo = fileInfo;

    this->children.clear();
}
void CDirTreeEntry::swap(CDirTreeEntry& other) {
    this->children.swap(other.children);
    m_fileName.swap(other.m_fileName);

    std::swap(m_fileInfo, other.m_fileInfo);
}

void CDirTree::buildTree(const CDirModel& selection) {
    QDir rootDirObj = selection.dir();
    QFileInfo rootInfo{ rootDirObj.absolutePath() };

    int numLevels = selection.levels();
    bool includeFiles = selection.includeFiles();

    this->dirRoot.assign(rootInfo);

    this->recursiveScan(this->dirRoot, rootDirObj, numLevels, includeFiles);
}
void CDirTree::recursiveScan(CDirTreeEntry& parent, const QDir& dir, int currLevel, bool includeFiles) {
    if ((currLevel > 0) && dir.isReadable()) {
        this->processDirectories(parent, dir, currLevel, includeFiles);

        if (includeFiles) {
            this->processFiles(parent, dir);
        }
    }
}
void CDirTree::processDirectories(CDirTreeEntry& parent, const QDir& dir, int currLevel, bool includeFiles) {
    auto& parentItem = parent.children;
    QDir::Filters filter = QDir::Dirs | QDir::NoDotAndDotDot;
    QDir::SortFlags sortFlags = QDir::Name;

    for (const QFileInfo& listItem : dir.entryInfoList(filter, sortFlags)) {
        QDir subDir{ listItem.absoluteFilePath() };
        CDirTreeEntry item{ listItem };

        this->recursiveScan(item, subDir, currLevel - 1, includeFiles);

        parentItem.appendChild(std::move(item));
    }
}
void CDirTree::processFiles(CDirTreeEntry& parent, const QDir& dir) {
    auto& parentItem = parent.children;
    QDir::Filters filter = QDir::Files;
    QDir::SortFlags sortFlags = QDir::Name;

    for (const QFileInfo& listItem : dir.entryInfoList(filter, sortFlags)) {
        CDirTreeEntry item{ listItem };

        parentItem.appendChild(std::move(item));
    }
}
