//#include <QFontMetric>
#include "DirSettings.h"


void DumpTree(const QTreeWidgetItem* root, const QString& fileName);
void WriteTreeToFile(const QTreeWidgetItem* root, const QString& fileName);
void DumpTreeNode(const QTreeWidgetItem* node, QTextStream& stream, const QString& prefix = QString());
bool IsNodeEnabled(const QTreeWidgetItem* node);
bool IncludeNodeChildren(const QTreeWidgetItem* node);
void DumpNodeChildren(const QTreeWidgetItem* node, QTextStream& stream, const QString& prefix);
QString GetSaveFile(QWidget* parent);

CDirSettings::CDirSettings(QWidget* parent, CDirModel* model) :
  QWidget{parent},
  m_model{model}
{
    this->setWindowTitle(tr("Settings"));

    this->Init();
}
void CDirSettings::Init(void) {
    this->CreateWidgets();
    this->SetupActions();
}
void CDirSettings::CreateWidgets(void) {
    auto layout = std::make_unique<QVBoxLayout>();

    this->CreateTreeInterface(layout.get());
    this->CreateOptionsInterface(layout.get());

    this->setLayout(layout.release());
}
void CDirSettings::SetupActions(void) {
    connect(m_treeWidget, SIGNAL(itemDoubleClicked(QTreeWidgetItem*, int)), this,
      SLOT(HandleItemDoubleClicked(QTreeWidgetItem*, int)));

    connect(m_backButton, SIGNAL(clicked()), this, SLOT(HandleBackButtonPress()));
    connect(m_refreshButton, SIGNAL(clicked()), this, SLOT(constructTree()));
    connect(m_dumpButton, SIGNAL(clicked()), this, SLOT(HandleDumpButtonPress()));
}
void CDirSettings::CreateTreeInterface(QBoxLayout* layout) {
    auto tree = std::make_unique<QTreeWidget>();

    tree->setSortingEnabled(false);
    tree->setHeaderLabel(tr("Name"));

    m_treeWidget = tree.get();

    layout->addWidget(tree.release());
}
void CDirSettings::CreateOptionsInterface(QBoxLayout* layout) {
    auto optionsLayout = std::make_unique<QHBoxLayout>();

    this->CreateBackButton(optionsLayout.get());
    this->CreateRefreshButton(optionsLayout.get());
    this->CreateDumpButton(optionsLayout.get());

    layout->addLayout(optionsLayout.release());
}
void CDirSettings::CreateBackButton(QBoxLayout* layout) {
    auto backButton = std::make_unique<QPushButton>();

    backButton->setText(tr("Back"));

    m_backButton = backButton.get();

    layout->addWidget(backButton.release());
}
void CDirSettings::CreateRefreshButton(QBoxLayout* layout) {
    auto refreshButton = std::make_unique<QPushButton>();

    refreshButton->setText(tr("Refresh"));

    m_refreshButton = refreshButton.get();

    layout->addWidget(refreshButton.release());
}
void CDirSettings::CreateDumpButton(QBoxLayout* layout) {
    auto dumpButton = std::make_unique<QPushButton>();

    dumpButton->setText(tr("Dump..."));

    m_dumpButton = dumpButton.get();

    layout->addWidget(dumpButton.release());
}
void CDirSettings::setModel(CDirModel* model) {
    m_model = model;
}
void CDirSettings::activate(void) {
    this->show();

    this->constructTree();
}
void CDirSettings::HandleBackButtonPress(void) const {
    this->EmitGoBackSignal();
}
void CDirSettings::EmitGoBackSignal(void) const {
    emit goBack();
}
void CDirSettings::HandleItemDoubleClicked(QTreeWidgetItem* item, int column) {
    Q_UNUSED(column);

    if (item->childCount() > 0) {
        this->ToggleChildIndicatorPolicy(item);
    }
}
void CDirSettings::ToggleChildIndicatorPolicy(QTreeWidgetItem* item) {
    bool expanded;
    QTreeWidgetItem::ChildIndicatorPolicy newPolicy;

    switch (item->childIndicatorPolicy()) {
        case QTreeWidgetItem::DontShowIndicator:
            newPolicy = QTreeWidgetItem::ShowIndicator;
            expanded = !item->isExpanded();
        break;
        default:
            newPolicy = QTreeWidgetItem::DontShowIndicator;
            expanded = item->isExpanded();
    }
    item->setChildIndicatorPolicy(newPolicy);
    item->setExpanded(expanded);
}
void CDirSettings::HandleDumpButtonPress(void) {
    const auto saveFile = GetSaveFile(this);
    const auto rootItem = m_treeWidget->topLevelItem(0);

    DumpTree(rootItem, saveFile);
}
void CDirSettings::constructTree(void) {
    if (m_model != nullptr) {
        m_model->refresh();

        m_dirTree.buildTree(*m_model);

        this->refreshTreeWidget();
    }
}
void CDirSettings::refreshTreeWidget(void) {
    auto treeRoot = this->addTreeItem(m_dirTree.dirRoot);

    m_treeWidget->clear();
    m_treeWidget->addTopLevelItem(treeRoot);

    //This must be called after the item has been added to the tree.
    treeRoot->setExpanded(true);
}
QTreeWidgetItem* CDirSettings::addTreeItem(const CDirTreeEntry& entry) {
    auto newChild = std::make_unique<QTreeWidgetItem>();
    auto iconType = QFileIconProvider::File;

    newChild->setText(0, entry.getName());

    if (entry.isDirectory()) {
        iconType = QFileIconProvider::Folder;

        this->fillSubTree(entry, newChild.get());
    }
    QIcon fileIcon = m_iconProvider.icon(iconType);
    newChild->setIcon(0, fileIcon);

    newChild->setFlags(Qt::ItemIsEnabled|Qt::ItemIsUserCheckable|Qt::ItemIsSelectable|Qt::ItemIsTristate);
    newChild->setCheckState(0, Qt::Checked);

    return newChild.release();
}
void CDirSettings::fillSubTree(const CDirTreeEntry& dirEntry, QTreeWidgetItem* treeItem) {
    for (const auto& childEntry : dirEntry.children) {
        auto item = this->addTreeItem(childEntry);

        treeItem->addChild(item);
    }
}
void DumpTree(const QTreeWidgetItem* root, const QString& fileName) {
    if (root != nullptr) {
        WriteTreeToFile(root, fileName);
    }
}
void WriteTreeToFile(const QTreeWidgetItem* root, const QString& fileName) {
    QFile outFile{ fileName };

    if (outFile.open(QIODevice::Truncate | QIODevice::WriteOnly)) {
        QTextStream stream{ &outFile };

//      DumpTreeNode(root, stream);
        if (IncludeNodeChildren(root)) {
            DumpNodeChildren(root, stream, "");
        }
    }
}
void DumpTreeNode(const QTreeWidgetItem* node, QTextStream& stream, const QString& prefix) {
    if (IsNodeEnabled(node)) {
        stream << prefix << node->text(0) << '\n';

        if (IncludeNodeChildren(node)) {
            DumpNodeChildren(node, stream, prefix + "    ");
        }
    }
}
bool IsNodeEnabled(const QTreeWidgetItem* node) {
    return (node->checkState(0) != Qt::Unchecked);
}
bool IncludeNodeChildren(const QTreeWidgetItem* node) {
    return ((node->childCount() > 0) &&
            (node->childIndicatorPolicy() != QTreeWidgetItem::DontShowIndicator));
}
void DumpNodeChildren(const QTreeWidgetItem* node, QTextStream& stream, const QString& prefix) {
    int numChildren = node->childCount();

    for (int i = 0; i < numChildren; ++i) {
        const auto child = node->child(i);

        DumpTreeNode(child, stream, prefix);
    }
}
QString GetSaveFile(QWidget* parent) {
    return QFileDialog::getSaveFileName(parent, QObject::tr("Save to..."), QString(), QObject::tr("Text files (*.txt)"));
}
