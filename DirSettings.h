#ifndef _DIRSETTINGS_H_INCLUDE_
#define _DIRSETTINGS_H_INCLUDE_


#include <QtWidgets>
#include <QFileDialog>
#include <QFile>
#include <QTextStream>
#include <QFileIconProvider>
#include <memory>
#include "DirModel.h"
#include "DirTree.h"


class CDirSettings : public QWidget {
    Q_OBJECT

private:
    CDirTree            m_dirTree;

    QFileIconProvider   m_iconProvider;

    CDirModel*          m_model;

    QTreeWidget*        m_treeWidget;

    QPushButton*        m_backButton;
    QPushButton*        m_refreshButton;
    QPushButton*        m_dumpButton;


    void Init(void);

    void CreateWidgets(void);
    void CreateTreeInterface(QBoxLayout* layout);
    void CreateOptionsInterface(QBoxLayout* layout);
    void CreateBackButton(QBoxLayout* layout);
    void CreateRefreshButton(QBoxLayout* layout);
    void CreateDumpButton(QBoxLayout* layout);

    void SetupActions(void);

    void refreshTreeWidget(void);
    void fillSubTree(const CDirTreeEntry& dirEntry, QTreeWidgetItem* treeItem);
    QTreeWidgetItem* addTreeItem(const CDirTreeEntry& entry);

    void ToggleChildIndicatorPolicy(QTreeWidgetItem* item);

    void EmitGoBackSignal(void) const;

signals:
    void goBack(void) const;

private slots:
    void HandleBackButtonPress(void) const;
    void HandleDumpButtonPress(void);
    void HandleItemDoubleClicked(QTreeWidgetItem*, int);

public slots:
    void constructTree(void);

public:
    CDirSettings(QWidget* parent = nullptr, CDirModel* model = nullptr);

    void setModel(CDirModel* model);

    void activate(void);
};

#endif
