#ifndef _DIRSELECT_H_INCLUDE_
#define _DIRSELECT_H_INCLUDE_

#include <QtCore>
#include <QtWidgets>
#include "smart_ptr.h"
#include "DirModel.h"
#include "FileSelector.h"

class CDirSelect : public QWidget {
    Q_OBJECT

private:
    CDirModel*        m_model;

    QPushButton*      m_nextButton;
    QSpinBox*         m_outputLevels;
    QCheckBox*        m_includeFiles;
    CFileSelector*    m_fileSelector;

    void Init(void);

    void SetupWidgets(void);
    void SetupActions(void);

    void CreateForm(QBoxLayout* layout);
    void CreateFileSelector(QBoxLayout* layout);
    void CreateOptionsMenu(QBoxLayout* layout);
    void CreateNextButton(QBoxLayout* layout);
    void CreateOutputLevels(QFormLayout* layout);
    void CreateIncludeFiles(QFormLayout* layout);

    int GetOutputLevels(void) const;
    bool GetIncludeFiles(void) const;

    void ProcessDir(void) const;

    void EmitDirSelected(const QDir &dir) const;

public slots:
    void HandleNextButtonPress(void) const;
    void HandleSelectedDir(const QString &dirName);

signals:
    void directorySelected(void) const;

public:
    CDirSelect(QWidget* parent = nullptr, CDirModel* model = nullptr);
//  ~CDirSelect();

    void setModel(CDirModel* model);
};

#endif
