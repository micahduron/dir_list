#ifndef _FILESELECTOR_H_INCLUDE_
#define _FILESELECTOR_H_INCLUDE_

#include <QtWidgets>
#include <QFontMetrics>
#include <QFileDialog>
#include "smart_ptr.h"

class CFileSelector : public QWidget {
    Q_OBJECT

private:
    QLineEdit*                  m_selectionText;
    QPushButton*                m_ellipsisButton;
    smart_ptr<QFileDialog>      m_fileDialog;

    QWidget*                    m_parent;
    QFileDialog::FileMode       m_fileMode;
    QFileDialog::Options        m_fileOptions;

    QFontMetrics                m_fontMetric;

    void EmitFileSelected(const QString &fileName) const;

    void Init(void);
    void SetupWidgets(void);
    void SetupActions(void);

    void CreateFileDialog(void);
    void CreateForm(QLayout* layout);
    void CreateSelectionText(QLayout* layout);
    void CreateEllipsisButton(QLayout* layout);

    QRect GetStringDimensions(const QString& str) const;

public slots:
    void HandleFileSelected(const QString &fileSelection);
    void HandleSelectionTextChange(const QString& formValue) const;

signals:
    void FileSelected(const QString &fileName) const;

public:
    CFileSelector(QWidget *parent                   = nullptr,
                  QFileDialog::FileMode fileMode    = QFileDialog::ExistingFile,
                  QFileDialog::Options miscOptions  = 0);
//  ~CFileSelector();

    QString selection(void) const;

    void setPlaceholderText(const QString& placeholder);
}; 
   
#endif 
