CONFIG += qt release c++14
QT += core widgets gui

QMAKE_CXXFLAGS_RELEASE = -Wall -Wpedantic -O2
QMAKE_CFLAGS_DEBUG = -g

HEADERS = DirList.h FileSelector.h DirSelect.h DirSettings.h DirModel.h DirTree.h smart_ptr.h
SOURCES = DirList.cpp FileSelector.cpp DirSelect.cpp DirSettings.cpp DirModel.cpp DirTree.cpp main.cpp
