#include "ErrorDialog.h"
#include "smart_ptr.h"

ED::ED(QWidget* parent) :
  m_parent(parent)
{
    this->setWindowTitle(tr("Errur"));
    this->setFixedSize(225, 100);

    this->Init();
}
void ED::Init(void) {
    this->SetupWidgets();
    this->SetupActions();
}
void ED::SetupWidgets(void) {
    smart_ptr<QVBoxLayout> tmpLayout(new QVBoxLayout(this));

    this->CreateDialog(tmpLayout.get());

    this->setLayout(tmpLayout.move());
}
void ED::CreateDialog(QBoxLayout* layout) {
    this->CreateErrorMessage(layout);
    this->CreateOkButton(layout);
}
void ED::CreateErrorMessage(QBoxLayout* layout) {
    smart_ptr<QLabel> tmpErrMsg(new QLabel);

    m_errMsg = tmpErrMsg.get();

    layout->addWidget(tmpErrMsg.move());
}
void ED::CreateOkButton(QBoxLayout* layout) {
    smart_ptr<QPushButton> tmpOk(new QPushButton);

    tmpOk->setText(tr("OK"));

    m_ok = tmpOk.get();

    layout->addWidget(tmpOk.move());
}
void ED::SetupActions(void) {
    connect(m_ok, SIGNAL(clicked()), this, SLOT(accept()));
}
void ED::setMessage(QString& message) {
    m_errMsg->setText(message);
}
void ED::showMessage(QString& message) {
    this->setMessage(message);

    this->exec();
}
void ShowErrorMessage(QString& errMsg) {
    CErrorDialog err;
