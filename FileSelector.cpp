#include "FileSelector.h"

CFileSelector::CFileSelector(QWidget *parent, QFileDialog::FileMode fileMode,
                                            QFileDialog::Options miscOptions) :
  QWidget{parent},
  m_selectionText{nullptr},
  m_ellipsisButton{nullptr},
  m_parent{parent},
  m_fileMode{fileMode},
  m_fileOptions{miscOptions},
  m_fontMetric{ this->font() }
{
    this->Init();
}
void CFileSelector::Init(void) {
    this->CreateFileDialog();
    this->SetupWidgets();
    this->SetupActions();
}
void CFileSelector::SetupWidgets(void) {
    auto tmpLayout = make_smart(new QHBoxLayout(this));

    this->CreateForm(tmpLayout.get());

    this->setLayout(tmpLayout.move());
}
void CFileSelector::CreateFileDialog(void) {
    auto tmpFileDialog = make_smart(new QFileDialog(m_parent));

    tmpFileDialog->setOptions(m_fileOptions);
    tmpFileDialog->setFileMode(m_fileMode);

    m_fileDialog = tmpFileDialog.move();
}
void CFileSelector::CreateForm(QLayout* layout) {
    this->CreateSelectionText(layout);
    this->CreateEllipsisButton(layout);
}
void CFileSelector::CreateSelectionText(QLayout* layout) {
    auto tmpSelectionText = make_smart(new QLineEdit);

    tmpSelectionText->setReadOnly(true);

    m_selectionText = tmpSelectionText.get();
    layout->addWidget(tmpSelectionText.move());
}
void CFileSelector::CreateEllipsisButton(QLayout* layout) {
    auto tmpEllipsisButton = make_smart(new QPushButton);
    QString buttonText(tr("..."));
    QRect strSize = this->GetStringDimensions(buttonText);

    tmpEllipsisButton->setText(buttonText);
    tmpEllipsisButton->setFixedWidth(strSize.width() + 6);

    m_ellipsisButton = tmpEllipsisButton.get();
    layout->addWidget(tmpEllipsisButton.move());
}
/*
void CFileSelector::SetupWidgets(QWidget *parent, QFileDialog::FileMode fileMode,
                                                QFileDialog::Options miscOptions)
{
    smart_ptr<QPushButton> tmpEllipsisButton(CreateEllipsisButton());
    smart_ptr<QLineEdit> tmpSelectionText(CreateSelectionText());
    smart_ptr<QHBoxLayout> layout(new QHBoxLayout(this));

    m_fileDialog = CreateFileDialog(parent, fileMode, miscOptions);

    m_ellipsisButton = tmpEllipsisButton.get();
    m_selectionText = tmpSelectionText.get();

    layout->addWidget(tmpSelectionText.move());
    layout->addWidget(tmpEllipsisButton.move());

    this->setLayout(layout.move());
}
*/
void CFileSelector::SetupActions(void) {
    connect(m_ellipsisButton, SIGNAL(clicked()), m_fileDialog.get(), SLOT(exec()));
    connect(m_fileDialog.get(), SIGNAL(fileSelected(const QString&)), this,
                           SLOT(HandleFileSelected(const QString&)));
    connect(m_selectionText, SIGNAL(textChanged(const QString&)), this,
                      SLOT(HandleSelectionTextChange(const QString&)));
}
void CFileSelector::HandleFileSelected(const QString &fileSelection) {
    m_selectionText->setText(fileSelection);
    m_fileDialog->close();
}
void CFileSelector::HandleSelectionTextChange(const QString &formValue) const {
    this->EmitFileSelected(formValue);
}
void CFileSelector::EmitFileSelected(const QString &value) const {
    emit FileSelected(value);
}
QString CFileSelector::selection(void) const {
    return m_selectionText->text();
}
void CFileSelector::setPlaceholderText(const QString& placeholder) {
    m_selectionText->setPlaceholderText(placeholder);
}
QRect CFileSelector::GetStringDimensions(const QString& str) const {
    int strWidth = m_fontMetric.width(str);
    int strHeight = m_fontMetric.height();

    return QRect(0, 0, strWidth, strHeight);
}
