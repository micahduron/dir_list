#include "DirSelect.h"
//#include "ErrorDialog.h"

CDirSelect::CDirSelect(QWidget* parent, CDirModel* model) :
  QWidget{parent},
  m_model{model}
{
    this->setWindowTitle(tr("Directory Dumper"));
    this->setFixedSize(225, 145);

    this->Init();
}
void CDirSelect::Init(void) {
    this->SetupWidgets();
    this->SetupActions();
}
void CDirSelect::SetupWidgets(void) {
    auto tmpLayout = make_smart(new QVBoxLayout{this});

    this->CreateForm(tmpLayout.get());

    this->setLayout(tmpLayout.move());
}
void CDirSelect::CreateForm(QBoxLayout* layout) {
    this->CreateFileSelector(layout);
    this->CreateOptionsMenu(layout);
    this->CreateNextButton(layout);
}
void CDirSelect::CreateFileSelector(QBoxLayout* layout) {
    auto tmpFileSelector =
      make_smart
        (
          new CFileSelector{this, QFileDialog::Directory, QFileDialog::ShowDirsOnly|QFileDialog::ReadOnly}
        );

    tmpFileSelector->setPlaceholderText(tr("Select input directory"));

    m_fileSelector = tmpFileSelector.get();
    layout->addWidget(tmpFileSelector.move(), Qt::AlignRight);
}
void CDirSelect::CreateOptionsMenu(QBoxLayout* layout) {
    auto tmpFormLayout = make_smart(new QFormLayout);

    this->CreateOutputLevels(tmpFormLayout.get());
    this->CreateIncludeFiles(tmpFormLayout.get());

    layout->addLayout(tmpFormLayout.move(), Qt::AlignLeft);
}
void CDirSelect::CreateNextButton(QBoxLayout* layout) {
    auto tmpNextButton = make_smart(new QPushButton);

    tmpNextButton->setText(tr("&Next"));
    tmpNextButton->setEnabled(false);

    m_nextButton = tmpNextButton.get();
    layout->addWidget(tmpNextButton.move(), Qt::AlignRight);
}
void CDirSelect::CreateOutputLevels(QFormLayout* layout) {
    auto tmpOutputLevels = make_smart(new QSpinBox);

    tmpOutputLevels->setMinimum(1);
    tmpOutputLevels->setMaximum(255);
    tmpOutputLevels->setSingleStep(1);
    tmpOutputLevels->setMinimumWidth(30);

    m_outputLevels = tmpOutputLevels.get();

    layout->addRow(tr("&Output levels up to:"), tmpOutputLevels.move());
}
void CDirSelect::CreateIncludeFiles(QFormLayout* layout) {
    auto tmpIncludeFiles = make_smart(new QCheckBox);

    tmpIncludeFiles->setTristate(false);

    m_includeFiles = tmpIncludeFiles.get();

    layout->addRow(tr("&Include files:"), tmpIncludeFiles.move());
}
void CDirSelect::SetupActions(void) {
    connect(m_nextButton, SIGNAL(clicked()), this, SLOT(HandleNextButtonPress()));

    connect(m_fileSelector, SIGNAL(FileSelected(const QString&)), this, SLOT(HandleSelectedDir(const QString&)));
}
void CDirSelect::EmitDirSelected(const QDir &dir) const {
    m_model->setLevels(this->GetOutputLevels());
    m_model->setIncludeFiles(this->GetIncludeFiles());
    m_model->setDir(dir);

    emit directorySelected();
}
void CDirSelect::HandleSelectedDir(const QString &formValue) {
    bool buttonEnabled = !formValue.isEmpty();

    m_nextButton->setEnabled(buttonEnabled);
}
void CDirSelect::HandleNextButtonPress(void) const {
    if (m_model != nullptr) {
        this->ProcessDir();
    }
    //Output an error otherwise?
}
void CDirSelect::ProcessDir(void) const {
    QDir selection(m_fileSelector->selection());

    if (!selection.exists()) {
        //this->ErrorDialog(tr("Directory does not exist."));
    } else if (!selection.isReadable()) {
        //this->ErrorDialog(tr("Directory is unreadable."));
    } else {
        this->EmitDirSelected(selection);
    }
}
int CDirSelect::GetOutputLevels(void) const {
    QString textVal = m_outputLevels->text();

    return textVal.toInt();
}
bool CDirSelect::GetIncludeFiles(void) const {
    return m_includeFiles->isChecked();
}
