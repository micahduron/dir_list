#include "DirEntry.h"


void CDirEntryList::appendChild(CDirEntry child) {
    m_children.push_back(std::move(child));
}
void CDirEntryList::swap(CDirEntryList& swapee) {
    m_children.swap(swapee);
}


void CDirEntry::setEntry(CDirEntry other) {
    this->swap(other);

    return *this;
}
void CDirEntry::swap(CDirEntry& other) {
    children.swap(other.children);
    m_fileName.swap(other.m_fileName);

    std::swap(m_enabled, other.m_enabled);
}
CDirEntry& operator=(CDirEntry other) {
    this->swap(other);

    return *this;
}
