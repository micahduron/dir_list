#ifndef _DIRTREE_H_INCLUDE_
#define _DIRTREE_H_INCLUDE_


#include <QDir>
#include <QFileInfo>
#include <vector>
#include <utility>
#include "DirModel.h"
//#include "FileName.h"



class CDirTree;
class CDirTreeEntry;

class CDirTreeEntryList {
private:
    typedef std::vector<CDirTreeEntry> list_type;


    list_type  m_children;


    void appendChild(CDirTreeEntry child);

    void clear(void)
        { m_children.clear(); }

    void swap(CDirTreeEntryList& swapee);

public:
    typedef list_type::size_type size_type;
    typedef list_type::iterator iterator;
    typedef list_type::const_iterator const_iterator;


    //Let the compiler define the copy constructor.
//  CDirTreeEntryList(const CDirTreeEntryList& other);

    size_type size(void) const
        { return m_children.size(); }

    iterator begin(void)
        { return m_children.begin(); }
    iterator end(void)
        { return m_children.end(); }
    const_iterator begin(void) const
        { return this->cbegin(); }
    const_iterator end(void) const
        { return this->cend(); }

    const_iterator cbegin(void) const
        { return m_children.cbegin(); }
    const_iterator cend(void) const
        { return m_children.cend(); }

    friend class CDirTreeEntry;
    friend class CDirTree;
};

class CDirTreeEntry {
private:
    QString             m_fileName;

    QFileInfo           m_fileInfo;


    void assign(const QFileInfo& fileInfo);
    void assign(CDirTreeEntry other)
        { this->swap(other); }
    void swap(CDirTreeEntry& other);

public:
    CDirTreeEntryList       children;


    CDirTreeEntry(void)
        {}
    CDirTreeEntry(const QFileInfo& fileInfo) :
      m_fileName{ fileInfo.fileName() },
      m_fileInfo{ fileInfo }
        {}
    CDirTreeEntry(const CDirTreeEntry& other) :
      m_fileName{ other.m_fileName },
      m_fileInfo{ other.m_fileInfo },
      children{ other.children }
        {}
    CDirTreeEntry(CDirTreeEntry&& other)
        { this->swap(other); }

    QString getName(void) const
        { return m_fileName; }

    QFileInfo getInfo(void) const
        { return m_fileInfo; }
    bool isDirectory(void) const
        { return m_fileInfo.isDir(); }
    bool isFile(void) const
        { return m_fileInfo.isFile(); }

    bool hasChildren(void) const
        { return this->isDirectory() && (this->children.size() > 0); }


    friend class CDirTree;
};

class CDirTree {
private:
    typedef std::vector<CDirTreeEntry> list_type;


    void recursiveScan(CDirTreeEntry& parent, const QDir& dir, int currLevel, bool includeFiles);
    void processDirectories(CDirTreeEntry& parent, const QDir& dir, int currLevel, bool includeFiles);
    void processFiles(CDirTreeEntry& parent, const QDir& dir);

public:
    CDirTreeEntry dirRoot;

    CDirTree(void) {}
    CDirTree(const CDirModel& dir)
        { this->buildTree(dir); }
    CDirTree(const CDirTree& other) :
      dirRoot{ other.dirRoot }
        {}
//  CDirTree(CDirTree&& other);

//  CDirTree& operator=(CDirTree other);

//  void swap(const CDirTree& other);

    void buildTree(const CDirModel& dir);
};

#endif
