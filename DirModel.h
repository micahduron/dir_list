#ifndef _DIRMODEL_H_INCLUDE_
#define _DIRMODEL_H_INCLUDE_

#include <QDir>
#include <QString>



class CDirModel {
private:
    QDir        m_dir;
    int         m_numLevels;
    bool        m_includeFiles;

public:
    CDirModel(void) :
      m_numLevels{1},
      m_includeFiles{false}
        {}
    CDirModel(const QDir& dir, int levels = 1, bool includeFiles = false);

    void setLevels(int levels)
        { m_numLevels = (levels > 0) ? levels : 1; }
    void setIncludeFiles(bool state)
        { m_includeFiles = state; }
    void setDir(const QDir& dir);

    int levels(void) const
        { return m_numLevels; }
    bool includeFiles(void) const
        { return m_includeFiles; }
    QDir dir(void) const
        { return m_dir; }

    void refresh(void)
        { m_dir.refresh(); }
};

#endif
