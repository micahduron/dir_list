#include "DirList.h"

CDirList::CDirList(void) :
  m_selectionDialog{ 0, &m_model },
  m_settingsDialog{ 0, &m_model }
{
    this->Init();
}
void CDirList::Init(void) {
    this->SetupActions();
}
void CDirList::SetupActions(void) {
    connect(&m_selectionDialog, SIGNAL(directorySelected()), this, SLOT(FinishedDirSelection()));
    connect(&m_settingsDialog, SIGNAL(goBack()), this, SLOT(HandleSettingsGoBack()));
}
void CDirList::FinishedDirSelection(void) {
    m_selectionDialog.hide();

    m_settingsDialog.activate();
}
void CDirList::HandleSettingsGoBack(void) {
    m_settingsDialog.hide();

    m_selectionDialog.show();
}
void CDirList::run(void) {
    m_selectionDialog.show();
}
