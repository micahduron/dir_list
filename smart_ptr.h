#ifndef _SMART_PTR_INCLUDE_H
#define _SMART_PTR_INCLUDE_H

#include <QAtomicPointer>

template <typename t_ptrType>
class smart_ptr {
public:
    typedef t_ptrType* ptr_type;
    typedef t_ptrType element_type;

private:
    QAtomicPointer<element_type> m_obj;

    void set(ptr_type otherPtr) { m_obj.store(otherPtr); }

    smart_ptr(const smart_ptr<element_type>& otherPtr) = delete;
    smart_ptr<t_ptrType>& operator=(const smart_ptr<element_type>& otherPtr) = delete;

public:
    smart_ptr(ptr_type obj = nullptr) : m_obj{obj} {}
    smart_ptr(smart_ptr<element_type>&& other)
        { this->swap(other); }
    ~smart_ptr() { this->release(); }

    smart_ptr<element_type>& operator=(ptr_type otherObj) {
        this->release();

        this->set(otherObj);

        return *this;
    }
    ptr_type get(void) const { return m_obj.load(); }
    ptr_type operator->(void) const { return this->get(); }

    void release(void) { delete this->get(); }

    void move(ptr_type* otherObj) {
        *otherObj = this->move();
    }
    ptr_type move(void) {
        return m_obj.fetchAndStoreOrdered(nullptr);
    }
    void swap(smart_ptr<element_type>& other) {
        ptr_type tempPtr = other.get();
        other = this->get();
        *this = tempPtr;
    }
};

template <class T>
smart_ptr<T> make_smart(T* ptr)
    { return smart_ptr<T>(ptr); }

#endif
