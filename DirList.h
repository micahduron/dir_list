#ifndef _DIRLIST_H_INCLUDE_
#define _DIRLIST_H_INCLUDE_

#include <QObject>
#include "DirModel.h"
#include "DirSelect.h"
#include "DirSettings.h"

class CDirList : public QObject {
    Q_OBJECT

private:
    CDirModel       m_model;
    CDirSelect      m_selectionDialog;
    CDirSettings     m_settingsDialog;

    void Init(void);

    void SetupActions(void);

    //Prevent the creation of copies.
    CDirList& operator=(const CDirList&) = delete;
    CDirList(const CDirList&) = delete;

public slots:
    void FinishedDirSelection(void);
    void HandleSettingsGoBack(void);

public:
    CDirList(void);
    void run(void);
};

#endif
