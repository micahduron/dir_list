#ifndef _DIRENTRY_H_INCLUDE_
#define _DIRENTRY_H_INCLUDE_

#include <QString>
#include <vector>
#include <utility>

class CDirModel;

class CDirEntryList {
private:
    typedef std::vector<CDirEntry> list_type;


    list_type  m_children;


    CDirEntryList(const CDirEntryList& other) = delete;

public:
    typedef list_type::size_type size_type;
    typedef list_type::iterator iterator;
    typedef list_type::const_iterator const_iterator;


    CDirEntryList(void) {}

    void appendChild(CDirEntry child);

    void clear(void)
        { m_children.clear(); }
    size_type size(void) const
        { return m_children.size(); }

    iterator begin(void) const
        { return m_children.begin(); }
    iterator end(void) const
        { return m_children.end(); }

    const_iterator cbegin(void) const
        { return m_children.cbegin(); }
    const_iterator cend(void) const
        { return m_children.cend(); }

    void swap(CDirEntryList& swapee);
};

class CDirEntry {
private:
    QString             m_fileName;
    bool                m_enabled;


    void setName(const QString& name)
        { m_fileName = name; }
    void swap(CDirEntry& other);

public:
    CDirEntryList       children;


    CDirEntry(void) :
      m_enabled{false}
        {}
    CDirEntry(const QString& name, bool enabled = true) :
      m_enabled{ enabled }
        { this->setName(name); }
    CDirEntry::CDirEntry(const CDirEntry& other) :
      m_fileName{ other.m_fileName },
      m_enabled{ other.m_enabled },
      children{ other.children }
        {}
    CDirEntry(CDirEntry&& other)
        { this->swap(other); }

    CDirEntry& operator=(CDirEntry other);

    void setEnabled(bool enable)
        { m_enabled = enable; }
    bool isEnabled(void) const
        { return m_enabled; }
    QString name(void) const
        { return m_fileName; }


    friend CDirModel;
};

#endif
