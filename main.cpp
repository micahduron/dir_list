#include <QApplication>
#include "DirList.h"

int RunApplication(QApplication* app);
int HandleGenericException(QApplication* app);

int main(int argc, char *argv[]) {
    QApplication app(argc, argv);
    int ret = 1;

    try {
        ret = RunApplication(&app);
    } catch (...) { /* TODO: Handle uncaught exceptions. */ }

    return ret;
}

int RunApplication(QApplication* app) {
    CDirList lister;

    lister.run();

    return app->exec();
}
