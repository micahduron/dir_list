#include "DirModel.h"



CDirModel::CDirModel(const QDir& dir, int levels, bool includeFiles) :
  m_dir{dir},
  m_includeFiles{includeFiles}
{
    this->setLevels(levels);
}
void CDirModel::setDir(const QDir& dir) {
    m_dir = dir;
}
