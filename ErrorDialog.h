#ifndef _ERRORDIALOG_H_INCLUDE_
#define _ERRORDIALOG_H_INCLUDE_

void ShowErrorDialog(QString& errMsg);

class CErrorDialog : public QDialog {
    Q_OBJECT

private:
    QWidget*        m_parent;

    QLabel*         m_errMsg;
    QPushButton*    m_ok;

    void Init(void);

    void SetupWidgets(void);
    void SetupActions(void);

    void CreateDialog(QBoxLayout* layout);
    void CreateErrorMessage(QBoxLayout* layout);
    void CreateOkButton(QBoxLayout* layout);

public:
    CErrorDialog(QWidget* parent = 0);

    void setMessage(QString& message);

    void showMessage(QString& errMsg);
};

#endif
